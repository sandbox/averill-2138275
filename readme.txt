Installation
------------

Install as any other module.

Setting up
----------

For this module to work it requires that several products are referenced from a product display(node). Each product
must have two options that are referenced from taxonomy terms. A simple explanation involves a T Shirt that is available
is the colours RED GREEN BLUE and sizes MED LARGE X-LARGE.

Once the module is installed, go to admin/commerce/products/types and select manage fields for the variation you want
have this module apply to. Edit the instance setting for both term fields. You should in the Attribute field settings
two extra settings, Multi add header row and Multi add rows. The multi add header applies to the table header so
if you select this for the colours in the example above, the table will show the colours across the top and the sizes
down the left side.

The Sizes and Colours in the example will be ordered relative to their order in the Taxonomy Term. If you want to
change the order, use the drag and drop interface there.

On the Manage display tab of your product display change the formatter for the product reference field to the new
Multi add to cart formatter and change the available settings.

You should be good to go.

If your product is for example not available in all colours and all sizes, a non editable field with N/A is placed in
the correct cell of the table.